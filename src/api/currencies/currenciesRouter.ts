import express from "express";
import { controllerHandler } from "../../shared/controllerHandler";
import { CurrenciesController } from "./currenciesController";
import { Request } from "express";

const router = express.Router();
const call = controllerHandler;
const Currency = new CurrenciesController();
router.get("/", call(Currency.index, (req: Request) => [req.query.type]));

export const CurrenciesRouter = router;