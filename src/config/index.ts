import * as dotenv from "dotenv";
dotenv.config();
export const PORT = process.env.PORT || "";
export const ENVIRONMENT = process.env.NODE_ENV || "";
export const APP_URL = process.env.APP_URL || "";
export const BASE_PATH = process.env.BASE_PATH || "";
export const CUR_HOST = process.env.CUR_HOST || "";
export const CUR_POST = process.env.CUR_POST || "";
export const CUR_ACTION = process.env.CUR_ACTION || "";
export const CURL_xmls = process.env.CURL_xmls || "";









