import 'mocha';
import { it } from "mocha";
import { expect } from "chai";
import supertest from "supertest"
import app from "../src/app";

const request = supertest(app);

describe("test init", function name() {
    it(`Response code must be 200`, async () => {
        const response = await request.get("/api/v1/currencies?type=Soap");
        expect(response.body.data).to.be.an("array");
        expect(response.status).to.be.equal(200);
    });
    it(`Response data must be array`, async () => {
        const response = await request.get("/api/v1/currencies?type=Soap");
        expect(response.body.data).to.be.an("array");
    });
    it(`Response code must be 400`, async () => {
        const response = await request.get("/api/v1/currencies?type=Soap2");
        expect(response.status).to.be.equal(400);
    });
    it(`Response code must be 404`, async () => {
        const response = await request.get("/api/v1/currencies2");
        expect(response.status).to.be.equal(404);
    });
});