# learned-integration

## Name
learned-integration

## Description
Integration (REST & SOAP)

## Installation
run npm i

## Test
run npm run test

## Usage
create .env in the project folder 
copy the content of env.example to the .env file
run npm run dev
navigate to http://localhost:3000/api/v1/currencies

## Support
akintundeakinwunmi@gmail.com

