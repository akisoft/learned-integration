
import axios from "axios"
import soapRequest from "easy-soap-request"
import xml2js from "xml-js";

export const Rest = async (url) => {
    const headers = {
        'Content-Type': 'application/json',
    }
    let res = await axios.get(url, { headers: headers })
    return res.data
}

export const SOAP = async (host: string, post: string, action: string, xmlns: string) => {
    try {
        let XML =
            `<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
            <soap12:Body>
            <${action} xmlns="${xmlns}">
            </${action}>
            </soap12:Body>
        </soap12:Envelope>`;

        let Headers = {
            "user-agent": "sampleTest",
            "Content-Type": "application/soap+xml; charset=utf-8",
            soapAction: `${host}${post}${action}`,
        };
        return new Promise((resolve, reject) => {
            soapRequest({
                url: `${host}${post}`,
                headers: Headers,
                xml: XML,
                timeout: 10000,
            }) // Optional timeout parameter(milliseconds)
                .then(({ response }: { response: any }) => {
                    const { body } = response;
                    const result = JSON.parse(
                        xml2js.xml2json(body, { compact: true, spaces: 4 })
                    );
                    const tCurrency = result["soap:Envelope"]["soap:Body"][`m:ListOfCurrenciesByNameResponse`][`m:ListOfCurrenciesByNameResult`]["m:tCurrency"]
                    let formatted_tCurrency = []
                    tCurrency.forEach(element => {
                        formatted_tCurrency.push(
                            {
                                "sISOCode": element["m:sISOCode"]._text,
                                "sName": element["m:sName"]._text
                            }
                        )
                    });
                    resolve(formatted_tCurrency);
                })
                .catch(reject);
        });
    } catch (error) {
        return "There was an error connecting to server"
    }
}

