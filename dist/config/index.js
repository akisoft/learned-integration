"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CURL_xmls = exports.CUR_ACTION = exports.CUR_POST = exports.CUR_HOST = exports.BASE_PATH = exports.APP_URL = exports.ENVIRONMENT = exports.PORT = void 0;
const dotenv = __importStar(require("dotenv"));
dotenv.config();
exports.PORT = process.env.PORT || "";
exports.ENVIRONMENT = process.env.NODE_ENV || "";
exports.APP_URL = process.env.APP_URL || "";
exports.BASE_PATH = process.env.BASE_PATH || "";
exports.CUR_HOST = process.env.CUR_HOST || "";
exports.CUR_POST = process.env.CUR_POST || "";
exports.CUR_ACTION = process.env.CUR_ACTION || "";
exports.CURL_xmls = process.env.CURL_xmls || "";
//# sourceMappingURL=index.js.map