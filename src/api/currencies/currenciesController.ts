import { BaseController } from "../baseController";
import { CurrenciesServices } from "./currenciesService";

export class CurrenciesController extends BaseController {

    private _currenciesService = new CurrenciesServices();

    public index = async (type: String = "Rest") => {
        return this.sendResponse(await this._currenciesService.index(type));
    }
}