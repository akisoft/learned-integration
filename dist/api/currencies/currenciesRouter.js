"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrenciesRouter = void 0;
const express_1 = __importDefault(require("express"));
const controllerHandler_1 = require("../../shared/controllerHandler");
const currenciesController_1 = require("./currenciesController");
const router = express_1.default.Router();
const call = controllerHandler_1.controllerHandler;
const Currency = new currenciesController_1.CurrenciesController();
router.get("/", call(Currency.index, (req) => [req.query.type]));
exports.CurrenciesRouter = router;
//# sourceMappingURL=currenciesRouter.js.map