"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrenciesServices = void 0;
const app_error_1 = require("../../utils/app-error");
const config_1 = require("../../config");
const fetcher_1 = require("../../utils/fetcher");
class CurrenciesServices {
    constructor() {
        this.index = async (type) => {
            if (type == "Rest") {
                return await (0, fetcher_1.Rest)(`${config_1.CUR_HOST}${config_1.CUR_POST}${config_1.CUR_ACTION}/JSON`);
            }
            else if (type == "Soap") {
                return await (0, fetcher_1.SOAP)(config_1.CUR_HOST, config_1.CUR_POST, config_1.CUR_ACTION, config_1.CURL_xmls);
            }
            else {
                throw new app_error_1.AppError("Invalid Request");
            }
        };
    }
}
exports.CurrenciesServices = CurrenciesServices;
//# sourceMappingURL=currenciesService.js.map