import { AppError } from "../../utils/app-error";
import { CUR_ACTION, CUR_HOST, CUR_POST, CURL_xmls } from "../../config";
import { Rest, SOAP } from "../../utils/fetcher";


export class CurrenciesServices {
    public index = async (type) => {
        if (type == "Rest") {
            return await Rest(`${CUR_HOST}${CUR_POST}${CUR_ACTION}/JSON`)
        } else if (type == "Soap") {
            return await SOAP(CUR_HOST, CUR_POST, CUR_ACTION, CURL_xmls)
        } else {
            throw new AppError("Invalid Request");
        }
    }
}