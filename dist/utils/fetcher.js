"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SOAP = exports.Rest = void 0;
const axios_1 = __importDefault(require("axios"));
const easy_soap_request_1 = __importDefault(require("easy-soap-request"));
const xml_js_1 = __importDefault(require("xml-js"));
const Rest = async (url) => {
    const headers = {
        'Content-Type': 'application/json',
    };
    let res = await axios_1.default.get(url, { headers: headers });
    return res.data;
};
exports.Rest = Rest;
const SOAP = async (host, post, action, xmlns) => {
    try {
        let XML = `<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
            <soap12:Body>
            <${action} xmlns="${xmlns}">
            </${action}>
            </soap12:Body>
        </soap12:Envelope>`;
        let Headers = {
            "user-agent": "sampleTest",
            "Content-Type": "application/soap+xml; charset=utf-8",
            soapAction: `${host}${post}${action}`,
        };
        return new Promise((resolve, reject) => {
            (0, easy_soap_request_1.default)({
                url: `${host}${post}`,
                headers: Headers,
                xml: XML,
                timeout: 10000,
            }) // Optional timeout parameter(milliseconds)
                .then(({ response }) => {
                const { body } = response;
                const result = JSON.parse(xml_js_1.default.xml2json(body, { compact: true, spaces: 4 }));
                const tCurrency = result["soap:Envelope"]["soap:Body"][`m:ListOfCurrenciesByNameResponse`][`m:ListOfCurrenciesByNameResult`]["m:tCurrency"];
                let formatted_tCurrency = [];
                tCurrency.forEach(element => {
                    formatted_tCurrency.push({
                        "sISOCode": element["m:sISOCode"]._text,
                        "sName": element["m:sName"]._text
                    });
                });
                resolve(formatted_tCurrency);
            })
                .catch(reject);
        });
    }
    catch (error) {
        return "There was an error connecting to server";
    }
};
exports.SOAP = SOAP;
//# sourceMappingURL=fetcher.js.map