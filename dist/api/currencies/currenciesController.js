"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrenciesController = void 0;
const baseController_1 = require("../baseController");
const currenciesService_1 = require("./currenciesService");
class CurrenciesController extends baseController_1.BaseController {
    constructor() {
        super(...arguments);
        this._currenciesService = new currenciesService_1.CurrenciesServices();
        this.index = async (type = "Rest") => {
            return this.sendResponse(await this._currenciesService.index(type));
        };
    }
}
exports.CurrenciesController = CurrenciesController;
//# sourceMappingURL=currenciesController.js.map